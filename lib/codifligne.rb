require "open-uri"
require "nokogiri"

require "codifligne/api"
require "codifligne/base"
require "codifligne/group_of_lines"
require "codifligne/line_notices"
require "codifligne/codifligne_error"
require "codifligne/line"
require "codifligne/network"
require "codifligne/operator"
require "codifligne/version"

module Codifligne
end
