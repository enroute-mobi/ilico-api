module Codifligne
  class Operator < Base
    attr_accessor :name, :stif_id, :xml, :default_contact, :private_contact, :customer_service_contact, :address

    def lines
      @lines ||= begin
        client = Codifligne::API.new
        client.lines(operator_name: self.name)
      end
    end
  end
end
