module Codifligne
  class GroupOfLines < Base
    attr_accessor :name, :status, :private_code, :stif_id, :line_codes, :transport_mode, :transport_submode, :xml
  end
end
