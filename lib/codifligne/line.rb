module Codifligne
  class Line < Base
    attr_accessor :transport_mode, :name, :short_name, :operator_codes, :stif_id, :status, :accessibility, :transport_submode, :xml, :operator_ref, :secondary_operator_ref, :seasonal, :private_code, :color, :text_color, :line_notices, :valid_from, :valid_until, :network_code

    def transport_mode
      @transport_mode&.to_s&.downcase
    end
  end
end
